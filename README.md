# [pandoc-pdf-template](https://gitlab.com/eidoom/pandoc-pdf-template)
* Derivates:
    * [statement-during-phd-md](https://gitlab.com/eidoom/statement-during-phd-md)
    * [phd-completion-statement](https://gitlab.com/eidoom/phd-completion-statement)
* Tested with `pandoc` `2.11.2`.
* Using [American Physical Society CSL](https://github.com/citation-style-language/styles/blob/master/american-physics-society.csl).
* [Live here](https://eidoom.gitlab.io/pandoc-pdf-template/document.pdf)
