DOCNAME=document

.PHONY: all view clean

all: $(DOCNAME).pdf

tmp.bib: $(DOCNAME).bib linkify.sh
	./$(word 2, $^) $< $@

tmp.yml: $(DOCNAME).yml dateify.sh
	./$(word 2, $^) $< $@

$(DOCNAME).pdf: $(DOCNAME).md tmp.bib tmp.yml american-physics-society.csl Makefile
	pandoc $< --bibliography=$(word 2, $^) --defaults=$(word 3, $^) --csl=$(word 4, $^) -o $@

view: $(DOCNAME).pdf
	xdg-open $< &

clean:
	-rm *.pdf
